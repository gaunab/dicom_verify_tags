#! env python3

from setuptools import setup

setup(name="dicomverifytags",
      version="0.1",
      description="Verify Dicom-TAGs within a study, to make sure all Dicoms are from the same person",
      author="Paul Kuntke",
      author_email="paul.kuntke@uniklinikum-dresden.de",
      install_requires=['pydicom', 'python-slugify'],
      packages=['dicomverifytags'],
      entry_points={'console_scripts': ['dicomverifytags=dicomverifytags.dicomverifytags:main',
                                        'dicomsettags=dicomverifytags.dicomsettags:main',
                                        'dicomverify_project=dicomverifytags.dicomverify_all_patients:main',
                                        'dicomrenamer=dicomverifytags.dicom_renamer:main'
                                        ]}
     )

