# dicom_verify_tags

Verify if all dicoms in a given directory match in their dicom tags. Goal is to get a simple way of prooving if all dicomfiles in a study are from the same person - as expected. 


# Setting Tags:

` dicomsettags` can be used to set any Tag in all files of a Dicom-Series to a specified value:
	
	dicomsettags PATH PathientsBirthDate=20200101



