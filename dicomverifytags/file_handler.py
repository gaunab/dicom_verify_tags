#! env python3
# -*- coding: utf-8 -*-

import os
import pydicom

def find_dicoms(paths):
    """ Find all dicom-files in subdirs of given path

    Parameters
    ----------
    path: str or list of str
        Directory that should be checked for dicoms

    Returns
    -------
    list of filenames

    """
    from re import split

    if type(paths) is str:
        paths = split('[ |,]', paths)

    dcmfiles = []

    for path in paths:
        for root, dirs, files in os.walk(path):
            for f in files:
                if not f.lower() == 'dicomdir':
                    fname = os.path.join(root, f)

                    try:
                        pydicom.dcmread(fname)
                        dcmfiles.append(fname)
                    except pydicom.errors.InvalidDicomError:
                        pass

    return dcmfiles

def tag_values(files, tags):
    """Fetch different values for all given files.

    This function returns all values for given dicom-tags. The values are
    queried by a list of files.

    Parameters
    ----------
    files: list of str
        Dicomfiles to iterate over
    tag: str or list of str
        dicomtags to search for

    Returns
    -------

    dict of values
    keys of dict represent the given tags and values of the dict are lists with
    all different values in the dicoms

    `{'name': ['Patient1', 'Patient2', 'Patient2']}`

    """

    values = {}
    # Fill all Values from dicom-files
    for f in files:
        dcm = pydicom.dcmread(f)
        for t in tags:
            if not t in values.keys():
                values[t] = []
            values[t].append(str(dcm.get(t)))


    return values


def set_tags(files, tags):
    """ Set Tags to given values in a list of files

    Parameters
    ----------

    files: list of str
        Dicomfiles to iterate over
    tags: dict of str
        Key: Tagname, Value: Value to write into tag
    """

    fileiterator = files
    try:
        from tqdm import tqdm
        fileterator = tqdm(files)
    except:
        pass

    for f in fileiterator:
        dcm = pydicom.dcmread(f)
        for key, value in tags.items():
            dcm[key].value = value
        pydicom.dcmwrite(f, dcm)



