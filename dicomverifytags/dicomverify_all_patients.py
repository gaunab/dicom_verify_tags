#! python

from .file_handler import tag_values, find_dicoms
from tqdm import tqdm
from glob import glob

def main():
    import argparse
    import sys
    parser = argparse.ArgumentParser(description="Check the dicom-Tags for each Patient in given Project-Dir")
    parser.add_argument( 'path', type=str, nargs=1,
                        help='Paths to find all dicoms')
    parser.add_argument('-l', '--list-tags', action='store_true', help='List all valid tags')
    parser.add_argument('-t', '--tags', type=str, nargs='+',     help='Tags for comparison')
    args = parser.parse_args()

    projectdir = str(args.path[0])
    conspicious = []
    pbar = tqdm(glob(f'{projectdir}/*/'))
    for patientdir in pbar:
        all_equal = True
        pbar.set_description(f"{patientdir.split('/')[-2]}")
        dicoms = find_dicoms(patientdir)
        values = tag_values(dicoms, args.tags)
        for tag, values in values.items():
            if len(set(values)) > 1:
                all_equal = False
        if not all_equal:
            print(f'Conspicious Patient: {patientdir.split("/")[-2]}')
            conspicious.append(patientdir)

    file=open('patients_to_check.txt', 'w')
    for pat in conspicious:
        file.writelines(pat+'\n')
    file.close()

if __name__ == "__main__":
    main()
