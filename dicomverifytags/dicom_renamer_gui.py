#! env python3
# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@uniklinikum-dresden.de"
__license__ = "BSD 3 Clause"

import sys
from os.path import sep, join, basename
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, qApp, QHBoxLayout, QVBoxLayout, QPushButton, QFileDialog, QGroupBox, QLineEdit, QProgressBar, QComboBox

try:
    from .dicom_renamer import  save_file
    from .file_handler import find_dicoms
except:
    from dicom_renamer import save_file
    from file_handler import find_dicoms

from PyQt5.QtCore import QSettings, pyqtSignal, QThread, QObject
from PyQt5.QtGui import QIcon
import PyQt5.QtCore as QtCore

tmpl_preset = '${PatientID}/${SeriesDescription}/${PatientID}_${SeriesDescription}'

class Dcmrengui(QMainWindow):

    def __init__(self):
        super().__init__()

        self.settings = QSettings("fgwmi", "dcmrenamer")

        self.initUI()


    def initUI(self):

        mainWidget = QWidget()

        exitAct = QAction(QIcon('exit.png'), '&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)

        self.statusBar()
        self.setWindowTitle('DicomRenamer')

        vbox = QVBoxLayout()
        hbox = QHBoxLayout()

        mainWidget.setLayout(vbox)
        self.setCentralWidget(mainWidget)

        self.dcmselector = DirSelector("Source", self.settings.value('sourcedir'))
        hbox.addWidget(self.dcmselector)

        self.outdirselector = DirSelector("Save to", self.settings.value('destdir'))
        hbox.addWidget(self.outdirselector)

        vbox.addLayout(hbox)

        templateBox = QHBoxLayout()

        self.edtOutfileTemplate = QLineEdit(self.settings.value('template', tmpl_preset))
        self.edtOutfileTemplate.setToolTip('Use any DicomTagName within braces ${} and marked with a $')
        templateBox.addWidget(self.edtOutfileTemplate)
        self.comboBox = QComboBox()
        self.comboBox.addItem(f"{sep} Directory Seperator", sep)
        self.comboBox.addItem("SeriesDescription", "${SeriesDescription}")
        self.comboBox.addItem("PatientName", "${PatientName}")
        self.comboBox.addItem("PatientID", "${PatientID}")
        self.comboBox.activated.connect(self.addToTemplate)
        templateBox.addWidget(self.comboBox)

        btnReset = QPushButton("Reset")
        btnReset.clicked.connect(self.resetTemplate)
        btnReset.setToolTip("Reset to default Template ")
        templateBox.addWidget(btnReset)

        vbox.addLayout(templateBox)

        btnRun = QPushButton("Convert")
        btnRun.clicked.connect(self.runConversion)
        vbox.addWidget(btnRun)

        self.pbar = QProgressBar()
        vbox.addWidget(self.pbar)

        self.setAcceptDrops(True)
        self.show()

    def addToTemplate(self, index):
        print(index)
        self.edtOutfileTemplate.setText(self.edtOutfileTemplate.text()  +   self.comboBox.itemData(index))

    def resetTemplate(self):
        self.statusBar().showMessage("Reset Filenametemplate", 2000)
        self.edtOutfileTemplate.setText(tmpl_preset)

    def runConversion(self):

        self.thread = QThread()
        self.worker = RenamingWorker(self.dcmselector.dirname, self.outdirselector.dirname, self.edtOutfileTemplate.text())

        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.range.connect(self.pbar.setRange)
        self.worker.progress.connect(self.pbar.setValue)
        self.worker.status.connect(self.statusBar().showMessage)
        self.settings.setValue('template', self.edtOutfileTemplate.text())
        self.settings.setValue('sourcedir', self.dcmselector.dirname)
        self.settings.setValue('destdir', self.outdirselector.dirname)


        self.thread.start()

class FileSelectorLineEdit(QLineEdit):
        def __init__(self):
            super().__init__()
            self.setAcceptDrops(True)
            self.setDragEnabled(True)

        def dropEvent(self, event):
            if event.mimeData().hasUrls():
                event.accept()
                for url in event.mimeData().urls():
                    path = url.toLocalFile()
                print(path)
            else:
                event.ignore()

            self.setText(path)

        def dragEnterEvent(self, event):
            if event.mimeData().hasUrls():
                event.accept()
            else:
                event.ignore()


class RenamingWorker(QObject):

    finished = pyqtSignal()
    range =  pyqtSignal(int, int)
    progress = pyqtSignal(int)
    status = pyqtSignal(str)

    def __init__(self, indir, outdir, template):
        super().__init__()
        self.indir = indir
        self.outdir = outdir
        self.template = template

    def __del__(self):
        self.wait()

    def run(self):
        self.status.emit('Searching DICOMs')
        files = find_dicoms(self.indir)
        self.range.emit(0,len(files))
        self.status.emit('Renaming DICOMS')
        for i in range(len(files)):
            self.progress.emit(i+1)
            fname = basename(files[i])
            appendix = '_' + fname.split('.')[0]
            save_file(files[i], join(self.outdir, self.template + appendix + '.dcm'))
        self.finished.emit()
        self.status.emit("Finished.")



class DirSelector(QGroupBox):
    def __init__(self, title="", defaultdir=None):
        super().__init__()

        self.setAcceptDrops(True)
        vbox = QVBoxLayout()
        self.setLayout(vbox)
        self.setTitle(title)
        self.edt_dirname = FileSelectorLineEdit()
        if not defaultdir is None:
            self.edt_dirname.setText(defaultdir)
        self.edt_dirname.setAcceptDrops(True)
        vbox.addWidget(self.edt_dirname)

        btn_select = QPushButton("Select Dir")
        btn_select.clicked.connect(self.selectDir)
        vbox.addWidget(btn_select)


    @property
    def dirname(self):
        return self.edt_dirname.text()

    def selectDir(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.Directory)
        dlg.setAcceptMode(QFileDialog.AcceptOpen)
        fname = dlg.getExistingDirectory(self, "Select Directory")
        self.edt_dirname.setText(fname)

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            for url in event.mimeData().urls():
                path = url.toLocalFile()
            print(path)
        else:
            event.ignore()

        self.edt_dirname.setText(path)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Dcmrengui()

    sys.exit(app.exec_())