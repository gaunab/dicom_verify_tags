#! env python3
# -*- coding: utf-8 -*-

__author__ = "Paul Kuntke"
__email__ = "paul.kuntke@uniklinikum-dresden.de"
__license__ = "BSD 3 Clause"


"""
Rename DICOM-Files by their tags. E.g. use SeriesDescription, StudyDescription
"""

import pydicom
from string import Template
from os.path import basename, join, dirname, isdir
from os import makedirs
try:
    from .file_handler import  find_dicoms
except:
    from file_handler import  find_dicoms

from argparse import ArgumentParser

def save_file(dicomfile: str, template: str = "${SeriesDescription}/${SeriesDescription}_"):
    """
    Actually save a dicom-file with a new filename/path.

    Examples
    --------
    save_file(

    Parameters
    ----------
    dicomfile: str
        Filename of the original file
    template: str
        Renaming-Template. Use {} to query for DicomTags

    Returns
    -------
    filename: str

    """
    tmpl = Template(template)
    dcm = pydicom.dcmread(dicomfile)

    tags = {}
    for tag in dcm.dir():
        tags[tag] = dcm.data_element(tag).value

    out_fname = tmpl.safe_substitute(tags) # Replace Tags
    out_fname = out_fname.replace(' ','_') # Replace spaces with _
    out_dir = dirname(out_fname)           # Check if dir exists if not, create dir
    if not isdir(out_dir):
        makedirs(out_dir)
    pydicom.dcmwrite(out_fname, dcm)

    return out_fname

def parse_args():
    parser = ArgumentParser(description="Rename DICOM-Files")
    parser.add_argument('path', type=str, nargs='+', help='Dicomdir')
    parser.add_argument('--template', type=str, help="Name-Template for output", default='${PatientID}/${SeriesDescription}/${PatientID}_${SeriesDescription}_${SliceLocation}')
    parser.add_argument('--remove_fileappendix', action='store_true', help="Activate this to remove the original_filename appendix")
    args = parser.parse_args()

    return args


def main():
    """
    Standalone Running

    Returns
    -------

    """

    args = parse_args()



    files = find_dicoms(args.path)
    for file in files:
        fname = basename(file)
        appendix = '_'+fname.split('.')[0]
        if args.remove_fileappendix:
            appendix=''

        save_file(file, args.template + appendix + '.dcm' )

if __name__ == '__main__':
    main()