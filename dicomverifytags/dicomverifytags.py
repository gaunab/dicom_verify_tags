#! python

from .file_handler import tag_values, find_dicoms






def main():
    import argparse
    import sys
    parser = argparse.ArgumentParser(description="Check the dicom-Tags of given Paths")
    parser.add_argument( 'path', type=str, nargs='+',
                        help='Paths to find all dicoms')
    parser.add_argument('-l', '--list-tags', action='store_true', help='List all valid tags')
    parser.add_argument('-t', '--tags', type=str, nargs='+',     help='Tags for comparison')
    args = parser.parse_args()

    dicoms = find_dicoms(args.path)
    values = tag_values(dicoms, args.tags)

    all_equal = True

    for tag, values in values.items():
        print("%s: " %tag)
        if len(set(values)) > 0:
            all_equal = False
        for value in set(values):
            print("\t* %s" %value)

    sys.exit(int(not all_equal))

if __name__ == "__main__":
    main()
