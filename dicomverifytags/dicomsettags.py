#! python

from .file_handler import set_tags, find_dicoms
import argparse
import sys



class StoreDictKeyPair(argparse.Action):
     def __call__(self, parser, namespace, values, option_string=None):
         my_dict = {}
         for kv in values.split(","):
             k,v = kv.split("=")
             my_dict[k] = v
         setattr(namespace, self.dest, my_dict)


def main():
    parser = argparse.ArgumentParser(description="Check the dicom-Tags of given Paths")
    parser.add_argument( 'path', type=str, nargs='+',
                        help='Paths to find all dicoms')
    parser.add_argument('-t', '--tags', dest="tags", action=StoreDictKeyPair, metavar="KEY1=VAL1,KEY2=VAL2...")

    args = parser.parse_args()

    dicoms = find_dicoms(args.path)
    set_tags(dicoms, args.tags)

if __name__ == "__main__":
    main()
